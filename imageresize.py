import os, sys, uuid
from PIL import Image
from uwwu import db, models

HORIZONTAL_LIMIT = 2000 
VERTICAL_LIMIT = 2000

FORMATS = {'.jpg', '.jpeg', '.png', '.webp'}

for infile in sys.argv[1:]:
    print('opening:', infile)
    path, name = os.path.split(infile)
    f,e = os.path.splitext(name)
    if e not in FORMATS:
        print('not a supported image... skipping')
        continue

    try:
        with Image.open(infile) as img:
            name = os.path.join(path, 'original', f + e)
            img.save(name)

            w,h = img.size
            if w > h:
                resize = HORIZONTAL_LIMIT / w
            else:
                resize = VERTICAL_LIMIT / h

            if resize >= 1:
                print('no resizing nessarry... skipping')
                continue

            if img.mode in ("RGBA", "P"):
                img = img.convert("RGB")

            name = os.path.join(path, f + '.jpg')

            w = int(w*resize)
            h = int(h*resize)

            img2 = img.resize((w, h))
            os.remove(infile)
            img2.save(name, 'jpeg', progressive=True)
            print('written', name)

            if e != '.jpg':
                try:
                    uid = uuid.UUID(int=f)
                except TypeError as err:
                    media = db.session.query(models.Media) \
		                      .filter(models.Media.url == 'media/'+f+e) \
				      .first()
                    uid = media.uuid
                try:
                    db.session.query(models.Media) \
	                      .filter(models.Media.uuid == uid) \
	                      .update({models.Media.url: 'media/'+f+'.jpg'})
                    db.session.commit()
                    print('png extension updated')
                except Exception as err:
                    print('error updating media extension')
                finally:
                    db.session.close()
		    
    except OSError as err:
        print(err)
    except ValueError as err:
        print(err)


