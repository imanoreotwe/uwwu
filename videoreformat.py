import os, sys, uuid, shutil
import ffmpeg
from uwwu import db, models

FORMATS = {'.webm'}
encode_queue = []

def encode_cleanup(processes):
    waiting = True
    while waiting:
        waiting = False
        for proc in processes:
            poll = proc[0].poll()
            if poll is None:
               waiting = True
            else:
                if poll > 0 or poll < 0:
                    print('error saving', proc[1], 'code:', poll)
                os.remove(proc[1])
                print('cleaned up', proc[1])
                processes.remove(proc)
    print('encoding complete')

for infile in sys.argv[1:]:
    print(infile)
    path, name = os.path.split(infile)
    f,e = os.path.splitext(name)
    if e not in FORMATS:
        print('not a webm.... dont care')
        continue

    try:
        save_name = os.path.join(path, 'original_vids', f + e)
        name = os.path.join(path, f + '.mp4')
        
        shutil.copyfile(infile, save_name) 
        print('encoding:', infile)
        proc = ffmpeg \
                .input(infile) \
                .output(name) \
                .run_async(pipe_stdout=False, pipe_stderr=True)
        encode_queue.append((proc, infile))

        if e != '.jpg':
            try:
                uid = uuid.UUID(int=f)
            except TypeError as err:
                media = db.session.query(models.Media) \
                          .filter(models.Media.url == 'media/'+f+e) \
                  .first()
                if media == None:
                    continue
                uid = media.uuid
            try:
                db.session.query(models.Media) \
                      .filter(models.Media.uuid == uid) \
                      .update({models.Media.url: 'media/'+f+'.mp4'})
                db.session.commit()
                print('extension updated')
            except Exception as err:
                print('error updating media extension')
            finally:
                db.session.close()

    except OSError as err:
        print(err)
    except ValueError as err:
        print(err)

encode_cleanup(encode_queue)


