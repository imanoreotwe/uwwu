from uwwu import db
from sqlalchemy.dialects.postgresql import UUID, SMALLINT, TIMESTAMP
from flask_sqlalchemy import SQLAlchemy
import uuid

class Media(db.Model):
    __tablename__ = 'medias'
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(UUID(as_uuid=True), default=uuid.uuid4(), unique=True, nullable=False)
    url = db.Column(db.String(), nullable=True)
    type = db.Column(SMALLINT, nullable=False)
    landscape = db.Column(SMALLINT, nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey('posts.id'), nullable=True)

    def __init__(self, uuid, url, type, landscape, post_id=None, id=None):
        self.id = id
        self.uuid = uuid
        self.url = url
        self.type = type 
        self.landscape = landscape
        self.post_id = post_id

class Post(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(UUID(as_uuid=True), default=uuid.uuid4(), unique=True, nullable=False)
    title = db.Column(db.String(), nullable=True, unique=True)
    hero_id = db.Column(db.Integer, db.ForeignKey('medias.id'), nullable=True)
    description = db.Column(db.String(), nullable=True)
    created = db.Column(TIMESTAMP, nullable=False)
    show_post = db.Column(SMALLINT, nullable=False)
    ref_id = db.Column(db.Integer, db.ForeignKey('refs.id'), nullable=True)
    notes = db.Column(db.String())

    def __init__(self, uuid, title, hero_id, description, created, show_post, notes, ref_id=None, id=None):
        self.id = id
        self.uuid = uuid
        self.title = title
        self.hero_id = hero_id
        self.description = description
        self.created = created
        self.show_post = show_post
        self.ref_id = ref_id
        self.notes = notes

class Tagmap(db.Model):
    __tablename__ = 'tagmap'
    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer, db.ForeignKey('posts.id'), nullable=False)
    tag_id = db.Column(db.Integer, db.ForeignKey('tags.id'), nullable=False)

    def __init__(self, post_id, tag_id, id=None):
        self.id = id
        self.post_id = post_id
        self.tag_id = tag_id

class Tag(db.Model):
    __tablename__ = 'tags'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)

    def __init__(self, name, id=None):
        self.id = id
        self.name = name

class Ref(db.Model):
    __tablename__ = 'refs'
    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String(), nullable=False)
    description = db.Column(db.String(), nullable=True)
    url = db.Column(db.String(), nullable=False)

    def __init__(self, site, description, url, id=None):
        self.id = id
        self.site = site
        self.description = description
        self.url = url
        

