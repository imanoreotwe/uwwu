from flask_wtf import FlaskForm
from wtforms import Form, RadioField, MultipleFileField, StringField, TextAreaField, validators


class PostForm(FlaskForm):
    title = StringField('Title', [validators.length(min=5, max=65), validators.InputRequired()])
    description = TextAreaField('Description')
    files = MultipleFileField('File(s)', [validators.InputRequired()])
    urls = TextAreaField('Video URLs')
    notes = TextAreaField('Notes')
