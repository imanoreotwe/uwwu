import os
import uuid
import json
import psycopg2.extras
import configparser 
import requests
from datetime import datetime
from config import Config
from forms import PostForm
from flask import Flask, render_template, url_for, request, redirect, send_from_directory, session
from authlib.integrations.flask_client import OAuth
from functools import wraps
from werkzeug.utils import secure_filename
from flask_sqlalchemy import SQLAlchemy
from markupsafe import escape
from flask_cdn import CDN

UPLOAD_FOLDER = 'static/media'
ALLOWED_EXTENTIONS = {'png', 'jpg', 'jpeg', 'gif', 'mp4', 'webm', 'webp'}
PICTURES_PER_PAGE = 20
SECRET_KEY = os.urandom(16)

app = Flask(__name__)
app.config.from_object(Config)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = SECRET_KEY
app.config['CDN_DOMAIN'] = 'uwwu.b-cdn.net'
app.config['CDN_TIMESTAMP'] = False 

CDN(app)
configs = configparser.RawConfigParser()
configs.read('secrets.conf')


db = SQLAlchemy(app)
import models

import admin
app.register_blueprint(admin.bp)

@app.errorhandler(500)
def internal_error(error):
    return 'YOU BROKE IT! jk don\'t worry something just went wrong. it happens.'

@app.errorhandler(404)
def internal_error(error):
    return 'Woah there. These are not the webpages you are looking for...' 

@app.route('/favicon.ico')
@app.route('/70194ed998c4fff2.html')
@app.route('/robots.txt')
def static_redirect():
    return send_from_directory(app.static_folder, request.path[1:])

@app.route('/')
def index():
    page_num = int(request.args.get('page', 1))
    results = db.session.query(models.Post) \
            .filter(models.Post.show_post == 1) \
            .order_by(models.Post.created.desc()) \
            .paginate(page_num, per_page=PICTURES_PER_PAGE)

    heros = []
    posts = {}
    for result in results.items:
        heros.append(db.session.query(models.Media).filter_by(id = result.hero_id)[0])
        posts[heros[-1]] = result

    results.items = heros
    
    return render_template('media.html', medias=results, posts=posts, title='latest')

@app.route('/2257')
def compliance():
    return render_template('2257.html', title='2257')

@app.route('/credits')
def credits():
    return render_template('credits.html', title='credits');

@app.route('/tags')
def tags():
    tags = db.session.query(models.Tag).order_by(models.Tag.name.asc()).all()
    return render_template('tags.html', tags=tags, title='tags')

@app.route('/t')
def t():
    return redirect('/tags')

@app.route('/t/<name>')
def tag_finder(name):
    page_num = int(request.args.get('page', 1))
    results = db.session.query(models.Tagmap, models.Post, models.Tag) \
                .filter(models.Tagmap.tag_id == models.Tag.id) \
                .filter(models.Tag.name.in_(name.split('+'))) \
                .filter(models.Tagmap.post_id == models.Post.id) \
                .filter(models.Post.show_post == 1) \
                .distinct(models.Post.created) \
                .order_by(models.Post.created.desc()) \
                .paginate(page_num, per_page=PICTURES_PER_PAGE)

    heros = []
    posts = {}
    for post in results.items:
        heros.append(db.session.query(models.Media).filter_by(id = post[1].hero_id)[0])
        posts[heros[-1]] = post[1]

    results.items = heros
    
    return render_template('media.html', name=name, medias=results, posts=posts, title=name)

@app.route('/p/<int:id>')
def posts(id):
    psycopg2.extras.register_uuid()
    post = db.session.execute('SELECT * FROM posts WHERE uuid = :val', {'val': uuid.UUID(int=id)}).first()
    if post is None:
        return render_template('post.html', medias=[], tags=[])

    # select all medias
    result = db.session.query(models.Post, models.Media) \
                .filter(models.Media.post_id == post.id) \
                .all()

    medias = [] 
    for row in result:
        if row[1] not in medias:
            medias.append(row[1])

    # get ref
    ref = db.session.query(models.Ref).filter(post.ref_id == models.Ref.id).first()

    # select post tags
    result = db.session.query(models.Tag, models.Tagmap) \
                .filter(post.id == models.Tagmap.post_id) \
                .filter(models.Tagmap.tag_id == models.Tag.id) \
                .order_by(models.Tag.name.asc()) \
                .all()

    tags = []
    for row in result:
        if row[0] not in tags:
            tags.append(row[0])

    related = related_posts(post.id, 9)

    return render_template('post.html', post=post, medias=medias, tags=tags, ref=ref, title=post.title, related=related)

@app.route('/gifs')
def gifs():
    page_num = int(request.args.get('page', 1))
    medias = db.session.query(models.Media) \
                .filter(models.Media.type == 1) \
                .order_by(models.Media.id.desc()) \
                .paginate(page_num, per_page=PICTURES_PER_PAGE)
    
    return render_template('media.html', medias=medias, posts=None, title='gifs')

@app.route('/pics')
def pics():
    page_num = int(request.args.get('page', 1))
    medias = db.session.query(models.Media) \
                .filter(models.Media.type == 0) \
                .order_by(models.Media.id.desc()) \
                .paginate(page_num, per_page=PICTURES_PER_PAGE)

    return render_template('media.html', medias=medias, posts=None, title='pics')

@app.route('/vids')
def vids():
    page_num = int(request.args.get('page', 1))
    medias = db.session.query(models.Media) \
                .filter((models.Media.type == 2) | (models.Media.type == 3)) \
                .order_by(models.Media.id.desc()) \
                .paginate(page_num, per_page=PICTURES_PER_PAGE)

    return render_template('media.html', medias=medias, posts=None, title='vids')

@app.route('/top')
def top():
    limit = 50
    psycopg2.extras.register_uuid()
    url = 'https://uwwu2.matomo.cloud/index.php?module=API&method=Actions.getPageUrls&idSite=1&period=year&date=yesterday&expanded=1&format=JSON&token_auth=' + configs.get('matomo', 'token')

    resp = requests.get(url)
    j = json.loads(resp.content)
    print(url)

    for i in range(0, len(j)):
        if 'p' == j[i]['label']:
            table = j[i]['subtable']
            break

    results = [] 
    for i in range(0, len(table)):
        if i == limit:
            break
        id = int(table[i]['label'][1:])
        post = db.session.execute('SELECT * FROM posts WHERE uuid = :val AND show_post = 1', {'val': uuid.UUID(int=id)}).first()
        if post is not None:
            results.append(post)
        else:
            limit += 1

    heros = []
    posts = {}
    for result in results:
        heros.append(db.session.query(models.Media).filter_by(id = result.hero_id)[0])
        posts[heros[-1]] = result

    return render_template('media_nopaginate.html', medias=heros, posts=posts, title='top')

def related_posts(post_id, rmax):
    ''' 
    results = db.session.query(models.Tagmap) \
                 .filter(models.Tagmap.post_id == post_id) \
                 .join(models.Tagmap, models.Tagmap.tag_id) \
                 .group_by(models.Tagmap.post_id) \
                 .all()
    '''

    statement = 'SELECT array_agg(map.tag_id) as tags, related.post_id \
                    FROM tagmap as map \
                        INNER JOIN tagmap as related ON map.post_id = :id AND map.tag_id=related.tag_id \
                        INNER JOIN posts ON posts.id = related.post_id AND posts.show_post = 1 \
                    WHERE related.post_id != :id AND posts.show_post = 1 \
                    GROUP BY related.post_id \
                    ORDER BY COUNT(related.tag_id) DESC \
                    LIMIT :max' 

    query = db.session.execute(statement, {'id': post_id, 'max': rmax})
    tmp = []
    results = []

    for row_number, row in enumerate(query):
        tmp.append({})
        for column_number, value in enumerate(row):
            tmp[row_number][row.keys()[column_number]] = value

    for r in tmp:
        uuid = db.session.query(models.Post).filter(models.Post.id == r['post_id']).first().uuid.int
        hero = db.session.query(models.Media).filter(models.Post.id == r['post_id']).filter(models.Media.id == models.Post.hero_id).first()
        hero_url = hero.url
        hero_type = hero.type
        p = {'uuid': uuid, 'hero_url': hero_url, 'type': hero_type}
        results.append(p) 

    return results

if __name__ == "__main__":
    app.run(host='0.0.0.0')
