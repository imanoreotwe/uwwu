const phone_only = 599;
const tablet_portrait = 600;
const tablet_landscape = 900;
const desktop = 1200;
const big_desktop = 1800;

var width = window.innerWidth;
var cols = 1;

if(width < phone_only) {
    cols = 1;
} else if(width > big_desktop) {
    cols = 5;
} else if(width > desktop) {
    cols = 4;
} else if(width > tablet_landscape) {
    cols = 3;
} else if(width > tablet_portrait) {
    cols = 2;
} 

// style edits
const wrapper = document.getElementById('wrapper');
wrapper.style.display = 'inline-block';
wrapper.style.height = 'auto';

// Build new divs
const posts = wrapper.children;


for(let i = 0; i < cols; i++) {
    let div = document.createElement('div');
    div.id = 'col-'+i;
    div.classList.add('posts-col');
    document.getElementById('wrapper').appendChild(div);
}


// Reorder divs
const len = document.getElementsByClassName('item').length;

for (let i = 0; i < len; i++) {
    let child = document.getElementById('post-'+i);
    document.getElementById('col-'+(i%cols)).appendChild( child );
    child.classList.remove('item-no-js');
}
