import os
import sys
import uuid
import functools
import traceback
import _thread
import ffmpeg
import traceback
import PIL
from PIL import Image
import psycopg2.extras
from config import Config
from forms import PostForm
from functools import wraps
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import secure_filename
from authlib.integrations.flask_client import OAuth
from uwwu import UPLOAD_FOLDER, ALLOWED_EXTENTIONS, app, db, models, PICTURES_PER_PAGE, configs
from flask import Flask, Blueprint, flash, g, redirect, render_template, request, session, url_for

HORIZONTAL_LIMIT = 2000
VERTICAL_LIMIT = 2000 

sys.path.append('/usr/local/bin/ffmpeg')

bp = Blueprint('admin', __name__, url_prefix='/admin')

oauth = OAuth(app)

auth0 = oauth.register(
     'auth0',
     client_id='56brekFcchrUuNQ7ApH2q9QVb3ooIE8B',
     client_secret=configs.get('auth0', 'secret'),
     api_base_url='https://thatdude.us.auth0.com',
     access_token_url='https://thatdude.us.auth0.com/oauth/token',
     authorize_url='https://thatdude.us.auth0.com/authorize',
     client_kwargs={
         'scope': 'openid profile email',
     },
 )


def requires_auth(f):
  @wraps(f)
  def decorated(*args, **kwargs):
    if 'profile' not in session:
      # Redirect to Login page here
      return redirect('/admin')
    return f(*args, **kwargs)
  return decorated

@bp.route('/')
def admin():
    return auth0.authorize_redirect(redirect_uri='')

@bp.route('/callback')
def callback():
    auth0.authorize_access_token()
    r = auth0.get('userinfo')
    userinfo = r.json()

    session['jwt_payload'] = userinfo
    session['profile'] = {
            'user_id': userinfo['sub'],
            'name': userinfo['name'],
            'picture': userinfo['picture']
    }

    return redirect('/admin/dashboard')


@bp.route('/dashboard')
@requires_auth
def dashboard():
    # create form
    form = PostForm()

    page_num = int(request.args.get('page', 1))
    results = db.session.query(models.Post) \
            .order_by(models.Post.created.desc()) \
            .paginate(page_num, per_page=PICTURES_PER_PAGE)

    heros = {}
    for result in results.items:
        heros[result.id] = db.session.query(models.Media).filter_by(id = result.hero_id)[0]
            
    return render_template('admin.html', form=form, posts=results, heros=heros, title='admin')

@bp.route('/post', methods=['POST'])
@requires_auth
def postpost():
    form = PostForm()
    print(form.title.data)
    if form.validate_on_submit():
        print('valid')
        try:
            handle_post(db.session, form)
            db.session.commit()
            print('post posted!')
        except Exception as e:
            db.session.rollback()
            print('exception caught', e)
            redirect('/admin/dashboard')
        finally:
            db.session.close()
    else:
        print('invalid data')

    return redirect('/admin/dashboard')

def encode_video(filename, output_filename):
    print('saving: ', output_filename)
    return ffmpeg \
            .input(filename) \
            .output(output_filename) \
            .run_async(pipe_stdout=False, pipe_stderr=True, cmd='/usr/local/bin/ffmpeg')

def encode_cleanup(processes):
    waiting = True
    while waiting:
        waiting = False
        for proc in processes:
            poll = proc[0].poll()
            if poll is None:
               waiting = True
            else:
                if poll > 0 or poll < 0:
                    print('error saving', proc[1], 'code:', poll)
                os.remove(proc[1])
                print('cleaned up', proc[1])
                processes.remove(proc)
    print('encoding complete')

def handle_post(session, form):
    post_uuid = uuid.uuid4()

    # add images
    if len(request.files) < 1:
        print('no files found')
        raise Exception('No files found.')

    files = form.files.data
    print('files', len(files))
    uploaded_files = []
    encoding_videos = []
    first_media = -1
    for file in files:
        #file = files[file]
        print(file.filename)

        # handle files
        if allowed_file(file.filename):
            file_uuid = uuid.uuid4()

            # do image transforms
            lnds = 0
            if 'video' in file.mimetype:
                filename = secure_filename(str(file_uuid.int) + '.mp4')
                filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)

                ext = os.path.splitext(file.filename)[1].lower()
                tmp_file = secure_filename(str(file_uuid.int) + ext)
                tmp_file = os.path.join(app.config['UPLOAD_FOLDER'], tmp_file)
                file.save(tmp_file)

                if '.mp4' not in file.filename:
                    proc = encode_video(tmp_file, filename)
                    encoding_videos.append((proc, tmp_file))
                else:
                    assert(filename == tmp_file)
                    print('saved:', tmp_file)
            else:
                if '.gif' in file.filename:
                    filename = secure_filename(str(file_uuid.int) + '.gif')
                    filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
                    file.save(filename)
                else:
                    try:
                        # check if landscape
                        img = Image.open(file)
                        w,h = img.size
                        if w > h:
                            lnds = 1
                            resize = HORIZONTAL_LIMIT / w
                        else:
                            resize = VERTICAL_LIMIT / h

                        # resize image if nedssarry
                        if resize < 1:
                            w = int(w*resize)
                            h = int(h*resize)
                            img = img.resize((w,h))

                        if img.mode in ("RGBA", "P"):
                            img = img.convert("RGB")

                        filename = secure_filename(str(file_uuid.int) + '.jpg')
                        filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
                        img.save(filename, 'jpeg', optimize=True, quality=70, progressive=True)
                        print('saved: ', filename)

                    except Exception as e:
                        print('could not edit image:', e)
                        raise e 

            uploaded_files.append(filename)

            # db entry
            file_type = filetype_to_int(filename)
            media = models.Media(
                    uuid = file_uuid,
                    url = filename[7:],
                    type = file_type,
                    landscape = lnds
                   )
            try:
                session.add(media)
                print('media row added')
            except Exception as e:
                print('error adding media to database:', e)
                delete_files(uploaded_files)
                raise e

            try:
                if first_media == -1:
                    first_media = session.query(models.Media).order_by(models.Media.id.desc()).first()
            except Exception as e:
                print('error retriving a hero id:', e)
                delete_files(uploaded_files)
                raise e
        elif file.filename == '':
            continue
        else:
            print('invalid file type:', file.filename)
            delete_files(uploaded_files)
            raise Exception('invalid file type')

    _thread.start_new_thread( encode_cleanup, (encoding_videos, ) )

    # do video urls
    if len(form.urls.data) > 0:
        urls = form.urls.data.split(',')
        for url in urls:
            url_uuid = uuid.uuid4()
            url = models.Media(
                    uuid = url_uuid,
                    url = url,
                    type = 3,
                    landscape = 1
                    )

            try:
                session.add(url)
                print('url row added')
            except Exception as e:
                print('error adding url to database:', e)
                raise e
    
    # post entry
    post_uuid = uuid.uuid4()
    post = models.Post(
            uuid = post_uuid,
            title = form.title.data,
            hero_id = first_media.id,
            description = form.description.data,
            created = datetime.now(),
            show_post = 0,
            notes = form.notes.data
            )

    try:
        session.add(post)
        session.commit() # I'd like to test without this here
        print('post added. id:', post.id, 'uuid:', post_uuid.int)
    except Exception as e:
        print('error adding post to database:', e)
        delete_files(uploaded_files)
        raise e

    try:
        session.query(models.Media) \
                .filter(models.Media.id >= first_media.id) \
                .update({models.Media.post_id: post.id})
        
    except Exception as e:
        print('error linking media ids with post id:', e)
        delete_files(uploaded_files)
        raise e
    

@bp.route('/edit', methods=['POST'])
@requires_auth
def edit():
    form = request.form
    # @TODO validate
    try:
        handle_edit(db.session, form)
        db.session.commit()
        print('edits made')
    except Exception as e:
        db.session.rollback()
        print('exception caught', e)
    finally:
        db.session.close()

    return redirect('/admin/dashboard')
         
def handle_edit(session, form):
     for k, v in form.items():
         print(k, v)

     for k, v in form.items():
         k = k.split('_')
         id = k[1]
         if 'delete' in k:
            delete_post(db.session, id)
         if 'rshow' in k:
            try:
                session.query(models.Post) \
                        .filter(models.Post.id == id) \
                        .update({models.Post.show_post: v})
            except Exception as e:
                print('error updating rows')
                raise e

def delete_post(session, id):
    try:
        files = session.query(models.Media) \
                        .filter(models.Media.post_id == id) \
                        .all()

        filenames = []
        for file in files:
            filenames.append(os.path.join('static', file.url))

        session.query(models.Post) \
                .filter(models.Post.id == id) \
                .delete()

        delete_files(filenames)
    except Exception as e:
        print('error deleting rows')
        raise e

@bp.route('/p/<int:id>', methods=['GET', 'POST'])
@requires_auth
def posts(id):
    if request.method == 'GET':
        psycopg2.extras.register_uuid()
        post = db.session.execute('SELECT * FROM posts WHERE uuid = :val', {'val': uuid.UUID(int=id)}).first()
        if post is None:
            return render_template('post.html', medias=[], tags=[])

        # select all medias
        result = db.session.query(models.Post, models.Media) \
                    .filter(models.Media.post_id == post.id) \
                    .all()

        medias = [] 
        for row in result:
            if row[1] not in medias:
                medias.append(row[1])

        # get ref
        ref = db.session.query(models.Ref).filter(post.ref_id == models.Ref.id).first()

        # select post tags
        result = db.session.query(models.Tag, models.Tagmap) \
                    .filter(post.id == models.Tagmap.post_id) \
                    .filter(models.Tagmap.tag_id == models.Tag.id) \
                    .order_by(models.Tag.name.asc()) \
                    .all()

        post_tags = []
        for row in result:
            if row[0] not in post_tags:
                post_tags.append(row[0])

        tags = db.session.query(models.Tag).all()
        return render_template('admin_post.html', post=post, medias=medias, post_tags=post_tags, ref=ref, tags=tags, title='admin')

    elif request.method == 'POST':
        form = request.form
        # @TODO validate
        try:
            handle_postedit(db.session, form, id)
            db.session.commit()
            print('edits made')
        except Exception as e:
            db.session.rollback()
            print('exception caught', e)
        finally:
            db.session.close()

    return redirect(request.url)

def handle_postedit(session, form, post_id):
    for k, v in form.items():
        print(k, v)
    
    post = db.session.execute('SELECT * FROM posts WHERE uuid = :val', {'val': uuid.UUID(int=post_id)}).first()
    if post is None:
        print('no post found!')
        redirect(request.url)
   
    selected_tags = []
    for k, v in form.items():
        k = k.split('_')
        if 'rhero' in k:
            try:
                session.query(models.Post) \
                        .filter(models.Post.id == post.id) \
                        .update({models.Post.hero_id: v})
            except Exception as e:
                print('error updating hero_id')
                raise e

        elif 'ctag' in k and v == 'on':
            selected_tags.append(int(k[1]))
            maps = session.query(models.Tagmap) \
                    .filter(models.Tagmap.post_id == post.id) \
                    .filter(models.Tagmap.tag_id == k[1]) \
                    .all()

            print(maps)

            if len(maps) == 0:
                mapping = models.Tagmap(
                        post_id = post.id,
                        tag_id = k[1]
                        )
                try:
                    session.add(mapping)
                    print('tagmapping added')
                except Exception as e:
                    print('error mapping tag to post')
                    raise e

    # delete all unselected tags
    print(selected_tags)
    try:
        count = session.query(models.Tagmap) \
            .filter(models.Tagmap.post_id == post.id) \
            .filter(~models.Tagmap.tag_id.in_(selected_tags)) \
            .delete(synchronize_session=False)
        print('unused tagmaps removed:', count)
    except Exception as e:
        print('error could not remove tagmap mappings')
        raise e

def allowed_file(file):
    return '.' in file and file.rsplit('.', 1)[1].lower() in ALLOWED_EXTENTIONS

def filetype_to_int(file):
    ext = '.' in file and file.rsplit('.', 1)[1].lower() 
    if ext in ['jpg', 'jpeg', 'gif', 'png', 'webp']:
        return 0;
    elif ext in ['mp4']:
        return 2
    elif ext in ['webm']:
        return 1
    return -1

def delete_files(files):
    for file in files:
        try:
            os.remove(file)
        except Exception as e:
            print('could not delete file', file, e)

