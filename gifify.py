import os, getopt

def main(argv):
    inputfile = ''
    offset = 0
    length = 0
    scale = 0
    pace = 1

    try:
        opts, args = getopt.getopt(argv, "hi:o:t:s:p:")
    except getopt.GetoptError:
        print('giffify.py -i <inputfile> -o <offset[s]> -t <length[s]> -s <scale> -p <pace>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('giffify.py -i <inputfile> -o <offset[s]> -t <length[s]> -s <scale> -p <pace>')
            sys.exit()

        if opt == '-i':
            inputfile = arg

        elif opt == '-o':
            offset = int(arg)

        elif opt == '-t':
            length = int(arg)

        elif opt == '-s':
            scale = int(arg)

        elif opt == '-p':
            pace = int(arg)
        
    os.system('ffmpeg -i {0}

if __name__ == "__main__":
   main(sys.argv[1:])